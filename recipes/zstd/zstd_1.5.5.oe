DESCRITPION = "Zstandard, or zstd as short version, is a fast lossless \
compression algorithm, targeting real-time compression scenarios at zlib-level \
and better compression ratios. It's backed by a very fast entropy stage, \
provided by Huff0 and FSE library."
LICENSE = "BSD-3-Clause & GPL-2.0-only"

RECIPE_TYPES = "machine native sdk"

SRC_URI = "git://github.com/facebook/zstd.git;protocol=https;${SRC_REV}"
SRC_REV = "tag=v${PV}"
S = "${SRCDIR}/zstd"

inherit meson

EXTRA_OEMESON += "-Dbin_programs=true -Dbin_contrib=true"

# We want to execute meson in the build/meson subdir
do_configure[dirs] += "${S}/build/meson"

DEPENDS += "libpthread"

inherit auto-package-libs
LIBRARY_VERSION = "1"
AUTO_PACKAGE_LIBS = "zstd"
AUTO_PACKAGE_LIBS_DEPENDS = "libc"
AUTO_PACKAGE_LIBS_RDEPENDS = "libc"
AUTO_PACKAGE_LIBS_PCPREFIX = "lib"
FILES_${PN}-libzstd-dev += "${includedir}"

inherit auto-package-utils
AUTO_PACKAGE_UTILS = "zstd pzstd zstdgrep zstdless zstd-frugal"
AUTO_PACKAGE_UTILS_DEPENDS = "libzstd"
AUTO_PACKAGE_UTILS_RDEPENDS = "libzstd"
DEPENDS_${PN}-pzstd += "libgcc libstdc++"
RDEPENDS_${PN}-pzstd += "libgcc libstdc++"
AUTO_PACKAGE_UTILS_SYMLINKS_zstd = "unzstd zstdcat zstdmt"
