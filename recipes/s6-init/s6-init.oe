DESCRIPTION = "Init system (PID 1) using s6 and s6-rc"

inherit s6rc
COMPATIBLE_IF_FLAGS = "s6rc"

RDEPENDS_${PN} += "s6 s6-linux-init"

SRC_URI = "file://init file://telinit file://shutdown \
	file://halt file://reboot file://poweroff"

SRC_URI += "file://rc.init file://rc.shutdown file://rc.shutdown.final \
	file://runlevel"

SRC_URI += "file://SIGINT file://SIGTERM file://SIGQUIT \
	file://SIGUSR1 file://SIGUSR2 file://SIGPWR file://SIGWINCH \
	file://crash file://finish"

FIRST_SERVICES = "s6-svscan-log s6-linux-init-runleveld s6-linux-init-shutdownd"
SRC_URI += "file://s6-svscan-log.run file://s6-svscan-log.notification-fd"
SRC_URI += "file://s6-linux-init-runleveld.run file://s6-linux-init-runleveld.notification-fd"
SRC_URI += "file://s6-linux-init-shutdownd.run"

S6RC_BUNDLE_SERVICES += "default shutdown"
RECIPE_FLAGS += "default_s6rc_bundle shutdown_s6rc_bundle"
DEFAULT_USE_default_s6rc_bundle = "system-watchdog"

S6RC_LONGRUN_SERVICES += "system-console"
SRC_URI += "file://system-console.run"
RECIPE_FLAGS += "system_console_s6rc_dependencies"
DEFAULT_USE_system_console_s6rc_dependencies = "\
	hostname init-coldplug init-utmp init-sysctl init-urandom \
	mount-proc mount-tmp mount-cgroups mount-all \
	syslogd klogd"
RDEPENDS_${PN} += "util/flock"

S6RC_LONGRUN_SERVICES += "system-watchdog"
SRC_URI += "file://system-watchdog.run"
RECIPE_FLAGS += "system_watchdog_s6rc_dependencies"
DEFAULT_USE_system_watchdog_s6rc_dependencies = "system-console"
RECIPE_FLAGS += "system_watchdog_device system_watchdog_options"
DEFAULT_USE_system_watchdog_device = "/dev/watchdog"
DEFAULT_USE_system_watchdog_options = "-t 1 -F"
do_configure[postfuncs] += "do_configure_system_watchdog"
do_configure_system_watchdog() {
	sed -e 's|DEVICE|${USE_system_watchdog_device}|' \
	    -e 's|OPTIONS|${USE_system_watchdog_options}|' \
	    -i ${SRCDIR}/system-watchdog.run
}

S6RC_ONESHOT_SERVICES += "hostname"
RECIPE_FLAGS += "hostname_s6rc_dependencies hostname_s6rc_timeout_up"
SRC_URI += "file://hostname.up"

S6RC_LONGRUN_SERVICES += "devd devd-log"
RECIPE_FLAGS += "devd_s6rc_dependencies"
DEFAULT_USE_devd_s6rc_dependencies = "init-devtmpfs mount-sys"
SRC_URI += "file://devd.run file://devd-log.pipeline-name \
	file://devd.producer-for file://devd-log.run \
	file://devd-log.consumer-for"
S6RC_NOTIFICATION_FD_devd = "3"
RDEPENDS_${PN} += "mdevd"

SRC_URI += " file://rtc.up file://rtc.down"
S6RC_ONESHOT_SERVICES += "rtc"
RECIPE_FLAGS += "rtc_s6rc_dependencies"
# default configuration relies on /dev/rtc symlink created by init-devtmpfs
DEFAULT_USE_rtc_s6rc_dependencies = "init-devtmpfs"
RDEPENDS_${PN} += "util/hwclock"

S6RC_ONESHOT_SERVICES += "init-coldplug"
RECIPE_FLAGS += "init_coldplug_s6rc_dependencies"
DEFAULT_USE_init_coldplug_s6rc_dependencies = "init-devtmpfs mount-sys devd"
RECIPE_FLAGS += "init_coldplug_s6rc_timeout_up"
SRC_URI += "file://init-coldplug.up"

S6RC_ONESHOT_SERVICES += "init-runtmpfs"
RECIPE_FLAGS += "init_runtmpfs_s6rc_dependencies"
RECIPE_FLAGS += "init_runtmpfs_s6rc_timeout_up"
SRC_URI += "file://init-runtmpfs.up"

S6RC_ONESHOT_SERVICES += "init-devtmpfs"
RECIPE_FLAGS += "init_devtmpfs_s6rc_dependencies"
DEFAULT_USE_init_devtmpfs_s6rc_dependencies = "init-runtmpfs"
RECIPE_FLAGS += "init_devtmpfs_s6rc_timeout_up"
SRC_URI += "file://init-devtmpfs.up"

S6RC_ONESHOT_SERVICES += "init-utmp"
RECIPE_FLAGS += "init_utmp_s6rc_dependencies"
DEFAULT_USE_init_utmp_s6rc_dependencies = "init-runtmpfs"
RECIPE_FLAGS += "init_utmp_s6rc_timeout_up"
SRC_URI += "file://init-utmp.up"
RECIPE_FLAGS += "wtmptrim"
INIT_UTMP_WTMPTRIM = ""
INIT_UTMP_WTMPTRIM:USE_wtmptrim = "do_patch_wtmptrim"
do_patch[postfuncs] += "${INIT_UTMP_WTMPTRIM}"
do_patch_wtmptrim() {
  sed -i -e 's/true wtmptrim/wtmptrim/' ${SRCDIR}/init-utmp.up
}
RDEPENDS_${PN}:>USE_wtmptrim = " util/wtmptrim"

S6RC_ONESHOT_SERVICES += "init-sysctl"
RECIPE_FLAGS += "init_sysctl_s6rc_dependencies"
DEFAULT_USE_init_sysctl_s6rc_dependencies = "mount-proc"
RECIPE_FLAGS += "init_sysctl_s6rc_timeout_up"
SRC_URI += "file://init-sysctl.up"

# Flag to use the real sysctl command for init-sysctl service
RECIPE_FLAGS += "s6_init_real_sysctl"
RDEPENDS_${PN}:>USE_s6_init_real_sysctl = " util/sysctl"

S6RC_ONESHOT_SERVICES += "init-urandom"
RECIPE_FLAGS += "init_urandom_s6rc_dependencies"
RECIPE_FLAGS += "init_urandom_s6rc_timeout_up"
SRC_URI += "file://init-urandom.up"

S6RC_ONESHOT_SERVICES += "mount-proc"
RECIPE_FLAGS += "mount_proc_s6rc_dependencies"
RECIPE_FLAGS += "mount_proc_s6rc_timeout_up"
SRC_URI += "file://mount-proc.up file://mount-proc.down"

S6RC_ONESHOT_SERVICES += "mount-sys"
RECIPE_FLAGS += "mount_sys_s6rc_dependencies"
RECIPE_FLAGS += "mount_sys_s6rc_timeout_up"
SRC_URI += "file://mount-sys.up file://mount-sys.down"

S6RC_ONESHOT_SERVICES += "mount-tmp"
RECIPE_FLAGS += "mount_tmp_s6rc_dependencies"
RECIPE_FLAGS += "mount_tmp_s6rc_timeout_up"
SRC_URI += "file://mount-tmp.up file://mount-tmp.down"

S6RC_ONESHOT_SERVICES += "mount-cgroups"
RECIPE_FLAGS += "mount_cgroups_s6rc_dependencies"
DEFAULT_USE_mount_cgroups_s6rc_dependencies = "mount-sys mount-proc"
RECIPE_FLAGS += "mount_cgroups_s6rc_timeout_up"
SRC_URI += "file://mount-cgroups.up"

S6RC_ONESHOT_SERVICES += "mount-all"
RECIPE_FLAGS += "mount_all_s6rc_dependencies"
DEFAULT_USE_mount_all_s6rc_dependencies = "init-coldplug"
RECIPE_FLAGS += "mount_all_s6rc_timeout_up"
SRC_URI += "file://mount-all.up"

S6RC_LONGRUN_SERVICES += "klogd-srv klogd-log"
RECIPE_FLAGS += "klogd_srv_s6rc_dependencies"
DEFAULT_USE_klogd_srv_s6rc_dependencies = "mount-proc"
SRC_URI += "file://klogd-srv.run \
	file://klogd-log.pipeline-name file://klogd-srv.producer-for \
	file://klogd-log.run file://klogd-log.consumer-for"

S6RC_LONGRUN_SERVICES += "syslogd-srv syslogd-log"
RECIPE_FLAGS += "syslogd_srv_s6rc_dependencies"
DEFAULT_USE_syslogd_srv_s6rc_dependencies = "init-devtmpfs"
SRC_URI += "file://syslogd-srv.run \
	file://syslogd-log.pipeline-name file://syslogd-srv.producer-for \
	file://syslogd-srv.notification-fd \
	file://syslogd-log.run file://syslogd-log.consumer-for \
	file://syslogd-srv.filter"
# Filter out TIMESTAMP part of RFC 3164 packet, as we are using s6-log
# timestamping instead
do_install_syslogd_srv_filter() {
	install -m 755 ${SRCDIR}/syslogd-srv.filter \
		${D}${s6rcsrcdir}/syslogd-srv/filter
}
do_install[postfuncs] += "do_install_syslogd_srv_filter"

RECIPE_FLAGS += "hostname"
do_configure[postfuncs] += "do_configure_hostname"
do_configure_hostname() {
	sed -i -e 's/HOSTNAME/${USE_hostname}/' ${SRCDIR}/hostname.up
}

SRC_URI += "file://s6-init-update file://s6-init-compile file://service"

do_install() {
	mkdir -p ${D}${base_sbindir} \
		 ${D}${sysconfdir}/s6-linux-init/current/run-image/service/.s6-svscan \
		 ${D}${sysconfdir}/s6-linux-init/current/env \
		 ${D}${localstatedir}/rc \
		 ${D}/run
        ln -sfT s6-linux-init/current/run-image/service ${D}${sysconfdir}/service
        ln -sfT s6-linux-init/current/env ${D}${sysconfdir}/env

        # Customized scripts originating from s6-linux-init-maker to /sbin
	for f in init telinit shutdown halt poweroff reboot ; do
	    install -D -m 0755 ${SRCDIR}/$f ${D}${sysconfdir}/s6-linux-init/current/bin/$f
	    ln -sfT ../${sysconfdir}/s6-linux-init/current/bin/$f ${D}${base_sbindir}/$f
	done
        # Customized scripts originating from s6-linux-init-maker
	for f in rc.init rc.shutdown rc.shutdown.final runlevel ; do
	    install -D -m 0755 ${SRCDIR}/$f ${D}${sysconfdir}/s6-linux-init/current/scripts/$f
	done

	for f in init s6-init-update s6-init-compile service ; do
	    install -D -m 0755 ${SRCDIR}/$f ${D}${base_sbindir}/$f
	done

	for f in SIGINT SIGTERM SIGQUIT SIGUSR1 SIGUSR2 SIGPWR SIGWINCH \
		crash finish
	do
		install -D -m 755 ${SRCDIR}/$f \
			${D}${sysconfdir}/s6-linux-init/current/run-image/service/.s6-svscan/$f
	done

	# Install pure s6 service directories
	for sv in ${FIRST_SERVICES}
	do
		mkdir -p ${D}${sysconfdir}/s6-linux-init/current/run-image/service/$sv
		install -m 755 ${SRCDIR}/$sv.run \
			${D}${sysconfdir}/s6-linux-init/current/run-image/service/$sv/run
                if [ -f ${SRCDIR}/$sv.notification-fd ] ; then
			install -m 755 ${SRCDIR}/$sv.notification-fd \
			        ${D}${sysconfdir}/s6-linux-init/current/run-image/service/$sv/notification-fd
                fi
	done
}

# Allow USE flag configuration of s6-log arguments
RECIPE_FLAGS += "devd_log_args klogd_log_args svscan_log_args syslogd_log_args"
DEFAULT_USE_devd_log_args = "s1000000 n20"
DEFAULT_USE_klogd_log_args = "s1000000 n20"
DEFAULT_USE_svscan_log_args = " "
DEFAULT_USE_syslogd_log_args = "s1000000 n20"
do_configure_s6_log() {
    sed -i -e "s|__args__|${USE_devd_log_args}|" ${SRCDIR}/devd-log.run
    sed -i -e "s|__args__|${USE_klogd_log_args}|" ${SRCDIR}/klogd-log.run
    sed -i -e "s|__args__|${USE_svscan_log_args}|" ${SRCDIR}/s6-svscan-log.run
    sed -i -e "s|__args__|${USE_syslogd_log_args}|" ${SRCDIR}/syslogd-log.run
}
do_configure[postfuncs] += "do_configure_s6_log"

SRC_URI += "file://devtable.txt"
inherit makedevs
MAKEDEVS_FILES = "${SRCDIR}/devtable.txt"

FILES_${PN} = "${base_sbindir} ${sysconfdir} ${localstatedir} /run"
RDEPENDS_${PN} += "execline s6 s6-rc s6-portable-utils s6-linux-utils"

PACKAGES = "${PN}-stage1 ${PN}-stage2 ${PN}-stage2-fallback ${PN}-stage2-finish ${PN}-stage3 ${PN}"
RDEPENDS_${PN} += "${PN}-stage1 ${PN}-stage2 ${PN}-stage2-fallback ${PN}-stage2-finish ${PN}-stage3"
FILES_${PN}-stage1 = "${base_sbindir}/init"
FILES_${PN}-stage2 = "${sysconfdir}/rc.init"
FILES_${PN}-stage2-fallback = "${sysconfdir}/rc.init-fallback"
FILES_${PN}-stage2-finish = "${sysconfdir}/rc.tini"
FILES_${PN}-stage3 = "${sysconfdir}/rc.shutdown"
