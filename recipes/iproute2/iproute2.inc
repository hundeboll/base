SUMMARY = "TCP / IP networking and traffic control utilities"
DESCRIPTION = "Iproute2 is a collection of utilities for controlling \
TCP / IP networking and traffic control in Linux.  Of the utilities ip \
and tc are the most important.  ip controls IPv4 and IPv6 \
configuration and tc stands for traffic control."
LICENSE = "GPL-2.0+"

COMPATIBLE_HOST_ARCHS = ".*linux"

inherit make pkgconfig auto-package-utils

SRC_URI="https://www.kernel.org/pub/linux/utils/net/${PN}/${PN}-${PV}.tar.xz"

DEPENDS += "libdl iptables-dev libmnl"

do_configure() {
  sh configure ${TARGET_SYSROOT}${includedir}
}

EXTRA_OEMAKE = "CC='${CC}' \
  KERNEL_INCLUDE=${TARGET_SYSROOT}${includedir} \
  LIBDIR='${libdir}' \
  SBINDIR='${base_sbindir}' \
  CONFDIR=${sysconfdir}/iproute2 \
  DOCDIR=${docdir}/iproute2 \
  MANDIR=${mandir} \
  SUBDIRS='lib tc ip bridge misc'"

AUTO_PACKAGE_UTILS = "bridge ip routel rtmon tc ifstat lnstat nstat rtacct ss"
AUTO_PACKAGE_UTILS_DEPENDS = "libc libdl libmnl"
AUTO_PACKAGE_UTILS_RDEPENDS = "libc libdl libmnl"

DEPENDS_${PN}-tc += "libm libxtables"
RDEPENDS_${PN}-tc += "libm libxtables"
DEPENDS_${PN}-ifstat += "libm"
RDEPENDS_${PN}-ifstat += "libm"
DEPENDS_${PN}-nstat += "libm"
RDEPENDS_${PN}-nstat += "libm"
DEPENDS_${PN}-rtacct += "libm"
RDEPENDS_${PN}-rtacct += "libm"

FILES_${PN}-tc += "${datadir}/bash-completion/completions/tc"
FILES_${PN}-tc += "${libdir}/tc/*"
FILES_${PN}-tc-dbg += "${libdir}/tc/.debug/*"
FILES_${PN}-lnstat += "${base_sbindir}/ctstat ${base_sbindir}/rtstat"

PACKAGES =+ " ${PN}-config"
FILES_${PN}-config = "${sysconfdir}/iproute2/*"

DEPENDS_${PN} = "${AUTO_PACKAGE_UTILS_PACKAGES}"
RDEPENDS_${PN} = "${AUTO_PACKAGE_UTILS_PACKAGES} ${PN}-config"

DEPENDS_${PN}-dbg = "${AUTO_PACKAGE_UTILS_DBG_PACKAGES}"
RDEPENDS_${PN}-dbg = "${AUTO_PACKAGE_UTILS_DBG_PACKAGES}"

PACKAGES += "${PN}-bash-completion"
FILES_${PN}-bash-completion = "${datadir}/bash-completion"
