DESCRIPTION = "GSSDP, A GObject-based API for handling resource discovery and announcement over SSDP."
LICENSE = "LGPL-2.0-or-later"

inherit library
inherit meson

RECIPE_TYPES = "machine"

COMPATIBLE_HOST_ARCHS = ".*linux"

require conf/fetch/gnome.conf
SRC_URI_SUBDIR = "${@'.'.join(d.getVar('PV', True).split('.')[:2])}"
SRC_URI = "${GNOME_MIRROR}/gssdp/${SRC_URI_SUBDIR}/gssdp-${PV}.tar.xz"

DEPENDS = "libglib-2.0 libgobject-2.0 libsoup libgio native:glib-utils"
DEPENDS_${PN} += "libsoup native:glib-utils"

FILES_${PN} = "${libdir}"

EXTRA_OEMESON += "-Dsniffer=false"
EXTRA_OEMESON += "-Dgtk_doc=false"
EXTRA_OEMESON += "-Dintrospection=false"
EXTRA_OEMESON += "-Dvapi=false"
EXTRA_OEMESON += "-Dexamples=false"
