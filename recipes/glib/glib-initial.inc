# -*- mode:python; -*-
require glib-common.inc

RECIPE_TYPES = "native"

PN = "glib"

DEPENDS += "${DEPENDS_PCRE}"
DEPENDS_PCRE += "libpcre2-8"
