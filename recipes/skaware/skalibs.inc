DESCRIPTION = "skalibs is a package centralizing the free software / \
open source C development files used for building all software at skarnet.org"

require skaware.inc

SRC_URI_base = "http://skarnet.org/software/${PN}/${PN}-${PV}.tar.gz"

RECIPE_FLAGS += "skalibs_srcrev"
SRC_URI_base:USE_skalibs_srcrev = "git://github.com/skarnet/skalibs.git;protocol=https;${USE_skalibs_srcrev}"
S:USE_skalibs_srcrev = "${SRCDIR}/${PN}"

SRC_URI = "${SRC_URI_base}"

RECIPE_TYPES = "machine native"

endianness = "${@['big', 'little'][d.get('HOST_ENDIAN') == 'l']}"

RECIPE_FLAGS += "skalibs_ipv6"
EXTRA_OECONF += "${EXTRA_OECONF_IPV6}"
EXTRA_OECONF_IPV6 = "--disable-ipv6"
EXTRA_OECONF_IPV6:USE_skalibs_ipv6 = "--enable-ipv6"

RECIPE_FLAGS += "skalibs_monotonic"
EXTRA_OECONF += "${EXTRA_OECONF_MONOTONIC}"
EXTRA_OECONF_MONOTONIC = "--disable-monotonic"
EXTRA_OECONF_MONOTONIC:USE_skalibs_monotonic = "--enable-monotonic"
DEFAULT_USE_skalibs_monotonic = True

EXTRA_OECONF += "${EXTRA_OECONF_DEVURANDOM}"
EXTRA_OECONF_DEVURANDOM = " --with-sysdep-devurandom=no"
EXTRA_OECONF_DEVURANDOM:>HOST_KERNEL_linux = " --with-sysdep-devurandom=yes"

do_configure() {
	./configure \
		--prefix=${prefix} \
		--dynlibdir=${sharedlibdir} \
		--libdir=${libdir} \
		--includedir=${includedir} \
		--datadir=${datadir} \
		--sysdepdir=${libdir}/skalibs/sysdeps \
		--with-default-path="${base_sbindir}:${sbindir}:${base_bindir}:${bindir}" \
		--host=${HOST_ARCH} \
		${EXTRA_OECONF}
}

PACKAGES = "${PN}-dbg ${PN}-dev ${PN}"
FILES_${PN}-dev += "${libdir}/skalibs/sysdeps"

inherit library
LIBRARY_NAME = "libskarnet"
LIBRARY_VERSION = "2"

DEPENDS_${PN} += "libc"
RDEPENDS_${PN} += "libc"
